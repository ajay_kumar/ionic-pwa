/**
 * Check out https://googlechromelabs.github.io/sw-toolbox/ for
 * more info on how to use sw-toolbox to custom configure your service worker.
 */


'use strict';
// importScripts('./build/sw-toolbox.js');

const cacheVersion = 'v008';

const cacheAssets = [
  './build/main.js',
  './build/vendor.js',
  './build/main.css',
  './build/polyfills.js',
  'index.html',
  'manifest.json'
];

self.addEventListener('install', (e) => {
  console.log('Service worker: Installed');

  e.waitUntil(
    caches.open(cacheVersion)
      .then((cache) => {
        console.log('Service worker: Caching files');
        cache.addAll(cacheAssets);
      })
      .then(() => {
        self.skipWaiting();
      })
  );
});

self.addEventListener('activate', (e) => {
  console.log('Service worker: activated');

  // Remove old caches
  e.waitUntil(
    caches.keys().then((versions) => {
      return Promise.all(versions.map((version) => {
        if (version !== cacheVersion) {
          console.log(`Clearing cache: ${version}`);
          return caches.delete(version);
        }
      }))
    })
  )
});

self.addEventListener('fetch', (e) => {
  console.log('Trying live server');
  e.respondWith(
    fetch(e.request)
      .then((res) => {
        console.log('caching response');
        const data = res.clone();
        caches.open(cacheVersion).then((cache) => {
          cache.put(e.request, data);
        });
        return res;
      })
      .catch(() => {
        console.log('Loading from cache');
        return caches.match(e.request)
      })
  )
});
